<?php
/**
 * Created by PhpStorm.
 * User: vchastinet
 * Date: 29/05/18
 * Time: 21:48
 */

namespace Domain\Model;


class CondicaoCotista
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $escolaridade;

    /**
     * @var float
     */
    private $renda;

    /**
     * @var string
     */
    private $etnia;

    /**
     * @var string
     */
    private $raca;

    /**
     * CondicaoCotista constructor.
     * @param string $escolaridade
     * @param float $renda
     * @param string $etnia
     * @param string $raca
     */
    public function __construct(
        $escolaridade,
        float $renda,
        string $etnia,
        string $raca
    ) {
        $this->escolaridade = $escolaridade;
        $this->renda = $renda;
        $this->$etnia = $etnia;
        $this->raca = $raca;
    }

    /**
     * @return string
     */
    public function getEscolaridade()
    {
        return $this->escolaridade;
    }

    /**
     * @return float
     */
    public function getRenda()
    {
        return $this->renda;
    }

    /**
     * @return string
     */
    public function getEtnia()
    {
        return $this->etnia;
    }

    /**
     * @return string
     */
    public function getRaca()
    {
        return $this->raca;
    }


}