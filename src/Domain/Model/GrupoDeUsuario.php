<?php
/**
 * Created by PhpStorm.
 * User: vchastinet
 * Date: 29/05/18
 * Time: 21:41
 */

namespace Domain\Model;


class GrupoDeUsuario
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $descricao;

    /**
     * GrupoDeUsuario constructor.
     * @param string $descricao
     */
    public function __construct(string $descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }


}