<?php
/**
 * Created by PhpStorm.
 * User: vchastinet
 * Date: 29/05/18
 * Time: 21:09
 */

namespace Domain\Model;

class Candidato
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cpf;

    /**
     * @var boolean
     */
    private $cotista;


    /**
     * @var string
     */
    private $telefone;

    /**
     * @var string
     */
    private $escolaridade;

    /**
     * @var float
     */
    private $renda;

    /**
     * @var string
     */
    private $etnia;

    /**
     * @var string
     */
    private $raca;

    /**
     * @var string
     */
    private $comprovacaoInstituicao;

    /**
     * @var string
     */
    private $declaracaoRenda;

    /**
     * Candidato constructor.
     * @param $cpf
     * @param $cotista
     * @param $telefone
     * @param $escolaridade
     * @param $renda
     * @param $etnia
     * @param $raca
     * @param $comprovacaoInstituicao
     * @param $declaracaoRenda
     */
    public function __construct(
        string $cpf,
        boolean $cotista,
        string $telefone,
        string $escolaridade,
        float $renda,
        string $etnia,
        string $raca,
        string $comprovacaoInstituicao,
        string $declaracaoRenda
    ) {
        $this->cpf = $cpf;
        $this->cotista = $cotista;
        $this->telefone = $telefone;
        $this->escolaridade = $escolaridade;
        $this->renda = $renda;
        $this->etnia = $etnia;
        $this->raca = $raca;
        $this->comprovacaoInstituicao = $comprovacaoInstituicao;
        $this->declaracaoRenda = $declaracaoRenda;
    }

    /**
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @return boolean
     */
    public function getCotista()
    {
        return $this->cotista;
    }

    /**
     * @return string
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @return string
     */
    public function getEscolaridade()
    {
        return $this->escolaridade;
    }

    /**
     * @return float
     */
    public function getRenda()
    {
        return $this->renda;
    }

    /**
     * @return string
     */
    public function getEtnia()
    {
        return $this->etnia;
    }

    /**
     * @return string
     */
    public function getRaca()
    {
        return $this->raca;
    }

    /**
     * @return string
     */
    public function getComprovacaoInstituicao()
    {
        return $this->comprovacaoInstituicao;
    }

    /**
     * @return mixed
     */
    public function getDeclaracaoRenda()
    {
        return $this->declaracaoRenda;
    }


}