<?php
/**
 * Created by PhpStorm.
 * User: vchastinet
 * Date: 29/05/18
 * Time: 21:45
 */

namespace Domain\Model;


class Usuario
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nome;

    /**
     * @var GrupoDeUsuario
     */
    private $grupo_id;

    /**
     * @var string
     */
    private $email;

    /**
     * Usuario constructor.
     * @param string $nome
     * @param GrupoDeUsuario $grupo_id
     * @param string $email
     */
    public function __construct(
        string $nome,
        GrupoDeUsuario $grupo_id,
        string $email
    ) {
        $this->nome = $nome;
        $this->grupo_id = $grupo_id;
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @return GrupoDeUsuario
     */
    public function getGrupoId()
    {
        return $this->grupo_id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }


}